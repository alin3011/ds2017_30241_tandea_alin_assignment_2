package com.sd.client.assign2.rmi.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import common.entities.Car;
import common.rmi.definition.ICarService;

public class CarService extends UnicastRemoteObject implements ICarService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7668946565300460594L;

	public CarService() throws RemoteException {
		super();
	}

	public double computeTax(Car c) throws RemoteException{
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if (c.getEngineCapacity() > 1601)
			sum = 18;
		if (c.getEngineCapacity() > 2001)
			sum = 72;
		if (c.getEngineCapacity() > 2601)
			sum = 144;
		if (c.getEngineCapacity() > 3001)
			sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}

	public double computePrice(Car c) throws RemoteException{
		double pricePurchasing = c.getPrice();
		int year = c.getYear();
		if (pricePurchasing > 0 && year > 0) {
			return pricePurchasing - (pricePurchasing / 7) * (2015 - year);
		} else {
			throw new RuntimeException("Invalid input");
		}
	}

}
