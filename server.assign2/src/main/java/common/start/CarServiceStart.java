package common.start;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.sd.client.assign2.rmi.impl.CarService;

import common.rmi.definition.ICarService;

public class CarServiceStart {

	public static void main(String[] args) {
		try {
			ICarService c = new CarService();
			Registry registry = LocateRegistry.createRegistry(1900);
			registry.rebind("rmi://localhost:1900/CarService", c);
			System.out.println("Server up");
		} catch (Exception e) {
			System.out.println("Exception is : " + e);
			e.printStackTrace();
		}
	}

}
