package com.sd.client.assign2.start;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import common.entities.Car;
import common.rmi.definition.ICarService;

public class ClientStart {

	private JFrame frame;
	private JTextField textFieldYear;
	private JTextField textFieldPrice;
	private JTextField textEngine;

	ICarService c;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientStart window = new ClientStart();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws MalformedURLException
	 */
	public ClientStart() throws MalformedURLException {
		try {
			//c = (ICarService) Naming.lookup("rmi://localhost:1900/CarService");
			 Registry registry = LocateRegistry.getRegistry(1900);
			c = (ICarService) registry.lookup("rmi://localhost:1900/CarService");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 578, 488);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textFieldYear = new JTextField();
		textFieldYear.setBounds(282, 60, 156, 20);
		frame.getContentPane().add(textFieldYear);
		textFieldYear.setColumns(10);

		textFieldPrice = new JTextField();
		textFieldPrice.setBounds(282, 130, 156, 20);
		frame.getContentPane().add(textFieldPrice);
		textFieldPrice.setColumns(10);

		textEngine = new JTextField();
		textEngine.setBounds(282, 199, 156, 20);
		frame.getContentPane().add(textEngine);
		textEngine.setColumns(10);

		JLabel lblFabricationYear = new JLabel("Fabrication year");
		lblFabricationYear.setBounds(87, 63, 139, 14);
		frame.getContentPane().add(lblFabricationYear);

		JLabel lblPurchasePrice = new JLabel("Purchase price");
		lblPurchasePrice.setBounds(87, 133, 139, 14);
		frame.getContentPane().add(lblPurchasePrice);

		JLabel lblEngineCapacity = new JLabel("Engine capacity");
		lblEngineCapacity.setBounds(87, 202, 139, 14);
		frame.getContentPane().add(lblEngineCapacity);

		final JTextPane textPane = new JTextPane();
		textPane.setBounds(120, 290, 274, 134);
		frame.getContentPane().add(textPane);

		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Car car = new Car();
				car.setYear(Integer.parseInt(textFieldYear.getText()));
				car.setPrice(Integer.parseInt(textFieldPrice.getText()));
				car.setEngineCapacity(Integer.parseInt(textEngine.getText()));
				double sellPrice = 0;
				double tax = 0;
				try {
					sellPrice = c.computePrice(car);
					tax = c.computeTax(car);
				} catch (RemoteException e) {
					e.printStackTrace();
				}

				String result = "\ntax : " + Double.toString(tax) + "\n" + "\nSell price : "
						+ Double.toString(sellPrice);
				textPane.setText(result);
			}
		});
		btnCalculate.setBounds(207, 256, 89, 23);
		frame.getContentPane().add(btnCalculate);
	}
}
