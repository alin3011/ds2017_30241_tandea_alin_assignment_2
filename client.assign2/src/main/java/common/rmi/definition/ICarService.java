package common.rmi.definition;

import java.rmi.Remote;
import java.rmi.RemoteException;

import common.entities.Car;

public interface ICarService extends Remote {
	public double computeTax(Car c) throws RemoteException;

	public double computePrice(Car c) throws RemoteException;
}
